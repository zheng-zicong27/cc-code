package com.cc.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cc.reggie.entity.DishFlavor;

/**
 * 菜品口味管理业务层
 *
 * @author 郑梓聪
 * @date 2022-06-22
 */
public interface DishFlavorService extends IService<DishFlavor> {

}
